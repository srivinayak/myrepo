

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
public class MyWebDriver
{
	WebDriver driver;

	@Given("Initialize My Chrome Driver")
	public void initialize_my_web_driver()
	{
		driver = new ChromeDriver();
	}

	@Then("Navigate to Google Web Site")
	public void launch_a_test_web_site()
	{
		driver.navigate().to("http://www.google.com");
	}

	@Then("Close My Web Driver")
	public void close_my_web_driver()
	{
		driver.quit();
	}

}
