import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;

//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features={"MyWebDriver.feature"}, format = {"pretty", "html:target/reports"} )//, glue = { "com.test.serenity.stepDefinitions" })

/**
 *
 */

/**
 * @author vinaysrinivasan
 *
 */
public class MySerenityCucumber {

	//@Steps
	MyWebDriver m1 = new MyWebDriver();
	//public static void main(String[] args) {


	public void start_my_web_driver()
	{
		m1.initialize_my_web_driver();
	}

	public void web_site_launch()
	{
		m1.launch_a_test_web_site();
	}

	public void web_driver_close()
	{
		m1.close_my_web_driver();
	}

	public static void main(String[] args) {

		MySerenityCucumber ms = new MySerenityCucumber();
		ms.start_my_web_driver();
		ms.web_site_launch();
		ms.web_driver_close();
	}


}
